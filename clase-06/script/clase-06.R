## Eduard Martinez
## update: 20-10-2022
## R version 4.1.1 (2021-08-10)

## clean environment
rm(list=ls())

## Instalar/llamar las librerías de la clase
require(pacman) 
p_load(tidyverse,rio,skimr,viridis)

##================================##
## **[0.] Configuración inicial** ##
##================================##

### **0.1 fijar semilla**
set.seed(101010)

### **0.2 leer datos**
house <- import("input/house_prices.rds") %>% na.omit()

### **0.3 partir la muestra**
sample_test <- sample(x = nrow(house) , size = nrow(house)*.2)

test <- house[sample_test,]

train <- house[-sample_test,]

##=======================##
## **[1.] Estimaciones** ##
##=======================##

### **1.1 Modelo con la constante**

## estimacion
specification1 <- lm(price~1,data=train) 

## summary
summary(specification1)

## coeficiente
coef(specification1)

## mean price
mean(train$price) 

## predicciones
test$specification1 <- predict(specification1 , newdata = test)

## MSE
mse1 <- with(test,mean((price-specification1)^2))
mse1

## Almacenar el resultado del modelo
models <- tibble(modelo=rep(NA,5),MSE=rep(NA,5))
models$modelo[1] = "Modelo 1"
models$MSE[1] = mse1

### **1.2 Puede mejorarse la predicción**
specification2 <- lm(price~as.factor(l3),data=train)

test$specification2<-predict(specification2,newdata = test) 

mse2 <- with(test,mean((price-specification2)^2))
mse2

models$modelo[2] = "Modelo 2"
models$MSE[2] = mse2

ggplot(data=models , aes(x=modelo , y=MSE , group=1)) + 
geom_point() + geom_line(col="red") + theme_test()

### **1.3 Que pasa si incluimos mas variables**

## modelo 3
specification3 <- lm(price~as.factor(l3)+rooms+bedrooms+bathrooms+surface_total,data=train) 

test$specification3<-predict(specification3,newdata = test) 

mse3 <- with(test,mean((price-specification3)^2,na.rm=T))
mse3

models$modelo[3] = "Modelo 3"
models$MSE[3] = mse3

ggplot(data=models , aes(x=modelo , y=MSE , group=1)) + 
geom_point() + geom_line(col="red") + theme_test()

## modelo 4
specification4 <- lm(price~as.factor(l3)+rooms+bedrooms+bathrooms+surface_total+lon+lat,data=train) 

test$specification4<-predict(specification4,newdata = test) 

mse4 <- with(test,mean((price-specification4)^2,na.rm=T))
mse4

models$modelo[4] = "Modelo 4"
models$MSE[4] = mse4

ggplot(data=models , aes(x=modelo , y=MSE , group=1)) + 
geom_point() + geom_line(col="red") + theme_test()

### **1.4 ¿Hay un límite para esta mejora?**
specification5 <- lm(price~as.factor(l3)+rooms+poly(bedrooms,2):poly(bathrooms,3)+surface_total+poly(lon,8):poly(lat,8),data=train) 

test$specification5<-predict(specification5,newdata = test) 

mse5 <- with(test,mean((price-specification5)^2,na.rm=T))
mse5

models$modelo[5] = "Modelo 5"
models$MSE[5] = mse5

ggplot(data=models , aes(x=modelo , y=MSE , group=1)) + 
geom_point() + geom_line(col="red") + theme_test()


